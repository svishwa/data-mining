from pyspark import SparkContext
import time
from collections import defaultdict
import math
import sys

mov_sim_dict = defaultdict(lambda x: 0)
rat_each_usr = defaultdict(dict)
nneighbors_dct = defaultdict(lambda x: 0)


def first_movie(mov_pair, sim):
    return mov_pair[0], (mov_pair[1], sim)


def cal_nearest_neighbors(mov1, mov_sim, n):
    mov_sim = sorted(mov_sim, key=lambda x: x[1], reverse=True)
    return mov1, mov_sim


def prediction(record):
    mv_id = record[0][1]
    act_usr = record[0][0]
    sum0 = 0.0
    sum1 = 0.0
    if mv_id in nneighbors_dct.keys():
        movies = nneighbors_dct[mv_id]
        for i in movies:
            if i[0] in rat_each_usr[act_usr].keys():
                sum0 += rat_each_usr[act_usr][i[0]] * i[1]
                sum1 += math.fabs(i[1])
        if sum1 != 0:
            val = sum0 / float(sum1)
            return act_usr, (mv_id, val)
        else:
            mvs = rat_each_usr[act_usr].items()
            sum0=0
            cnt=0
            for i in mvs:
                sum0+=i[1]
                cnt+=1
            return act_usr,(mv_id,sum0/float(cnt))

    else:
        mvs = rat_each_usr[act_usr].items()
        sum0=0
        cnt=0
        for i in mvs:
            sum0+=i[1]
            cnt+=1
        return act_usr,(mv_id,sum0/float(cnt))


if __name__ == '__main__':
    sc = SparkContext('local[*]')
    start = time.time()
    RDD = sc.textFile(sys.argv[1])
    RDD = RDD.map(lambda x: x.split(","))
    header = RDD.first()
    RDD = RDD.filter(lambda x: x != header)

    totRDD = RDD.map(lambda x: ((int(x[0]), int(x[1])), float(x[2])))
    predRDD = sc.textFile(sys.argv[2])
    predRDD = predRDD.map(lambda x: x.split(","))
    header1 = predRDD.first()
    predRDD = predRDD.filter(lambda x: x != header1)
    predRDD = predRDD.map(lambda x: ((int(x[0]), int(x[1])), 1.0))
    RDD = totRDD.subtractByKey(predRDD).map(lambda x: (x[0][0], x[0][1], x[1]))
    fp = open(sys.argv[3])
    lines = fp.read().split('\n')
    fp.close()
    for i in lines:
        lst = list()
        lst = i.split(',')
        lst = [x for x in lst if x]
        if len(lst) != 0:
            m1 = int(lst[0])
            m2 = int(lst[1])
            sim = float(lst[2])
            mov_sim_dict[(m1, m2)] = sim
    lst = RDD.collect()
    for (usr_id, mov_id, rat) in lst:
        rat_each_usr[usr_id][mov_id] = rat
    item_sim_RDD = sc.parallelize(mov_sim_dict.items())
    # print item_sim_RDD.count()
    item_sim_RDD = item_sim_RDD.map(lambda x: first_movie(x[0], x[1])).groupByKey().sortByKey()
    item_sim_RDD = item_sim_RDD.map(lambda x: (x[0], list(x[1])))
    item_sim_RDD = item_sim_RDD.map(lambda x: cal_nearest_neighbors(x[0], x[1], 50)).sortByKey().cache()
    print item_sim_RDD.count()
    nneighbors_dct = item_sim_RDD.collectAsMap()
    finRDD = predRDD.map(lambda x: prediction(x)).filter(bool)
    RDD1 = finRDD.map(lambda x: ((x[0], x[1][0]), x[1][1]))
    tot_res_RDD = totRDD.join(RDD1)
    cal_RDD = tot_res_RDD.map(lambda r: ((r[0]), math.fabs(r[1][0] - r[1][1]))).sortByKey()
    MSE = tot_res_RDD.map(lambda r: (r[1][0] - r[1][1]) ** 2).mean()
    print("Root Mean Squared Error = " + str(math.sqrt(MSE)))
    cnt1 = cal_RDD.filter(lambda x: 0 <= x[1] < 1.0).count()
    cnt2 = cal_RDD.filter(lambda x: 1.0 <= x[1] < 2.0).count()
    cnt3 = cal_RDD.filter(lambda x: 2.0 <= x[1] < 3.0).count()
    cnt4 = cal_RDD.filter(lambda x: 3.0 <= x[1] < 4.0).count()
    cnt5 = cal_RDD.filter(lambda x: x[1] >= 4.0).count()
    print ">=0 and < 1:",cnt1
    print ">=1 and < 2:",cnt2
    print ">=2 and < 3:",cnt3
    print ">=3 and < 4:",cnt4
    print ">=4:",cnt5
    resRDD = finRDD.groupByKey().map(lambda x: (x[0], list(x[1]))).sortByKey()
    res_lst = resRDD.collect()
    f = open('Sandesh_Vishwanath_ItemBasedCF.txt', 'w+')
    for i in res_lst:
        lst = sorted(i[1], key=lambda x: x[0])
        for k in lst:
            f.write(str(i[0]) + "," + str(k[0]) + "," + str(k[1]) + "\n")
    f.close()
    end = time.time()
    print "time:", end - start
