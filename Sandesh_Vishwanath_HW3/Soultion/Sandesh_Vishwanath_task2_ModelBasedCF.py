from pyspark import SparkContext
import math
from pyspark.mllib.recommendation import ALS, Rating
import time
import sys


def get_key(x):
    return "{0}{1}".format(x[0], x[1])


if __name__ == '__main__':
    sc = SparkContext('local[*]')
    start =time.time()
    totRDD = sc.textFile(sys.argv[1])
    totRDD = totRDD.map(lambda x: x.split(","))
    header = totRDD.first()
    totRDD = totRDD.filter(lambda x: x != header)
    totRDD = totRDD.map(lambda x: ((int(x[0]), int(x[1])), float(x[2])))
    predRDD = sc.textFile(sys.argv[2])
    predRDD = predRDD.map(lambda x: x.split(","))
    header1 = predRDD.first()
    predRDD = predRDD.filter(lambda x: x != header1)
    predRDD = predRDD.map(lambda x: ((int(x[0]), int(x[1])), 1.0))
    RDD = totRDD.subtractByKey(predRDD).map(lambda x: Rating(x[0][0], x[0][1], x[1]))
    rank = 7
    numIterations = 10
    model = ALS.train(RDD, rank, numIterations)
    testdata = predRDD.map(lambda p: (p[0][0], p[0][1]))
    predictions = model.predictAll(testdata).map(lambda r: ((r[0], r[1]), r[2]))
    print predictions.count()
    result_lst = predictions.collect()
    result_lst.sort(key=lambda x: x[0])
    f = open('model_result_big.txt','w+')
    for i in result_lst:
        f.write(str(i[0][0]) +"," + str(i[0][1])+","+str(i[1])+"\n")
    ratesAndPreds = totRDD.map(lambda r: ((r[0][0], r[0][1]), r[1])).join(predictions)
    finRDD = ratesAndPreds.map(lambda r: ((r[0][0], r[0][1]), math.fabs(r[1][0] - r[1][1]))).sortByKey()
    print finRDD.take(2)
    MSE = ratesAndPreds.map(lambda r: (r[1][0] - r[1][1]) ** 2).mean()
    print("Root Mean Squared Error = " + str(math.sqrt(MSE)))
    cnt1 = finRDD.filter(lambda x: 0 <= x[1] < 1.0).count()
    cnt2 = finRDD.filter(lambda x: 1.0 <= x[1] < 2.0).count()
    cnt3 = finRDD.filter(lambda x: 2.0 <= x[1] < 3.0).count()
    cnt4 = finRDD.filter(lambda x: 3.0 <= x[1] < 4.0).count()
    cnt5 = finRDD.filter(lambda x: x[1] >= 4.0).count()
    print ">=0 and < 1:",cnt1
    print ">=1 and < 2:",cnt2
    print ">=2 and < 3:",cnt3
    print ">=3 and < 4:",cnt4
    print ">=4:",cnt5
    end = time.time()
    print "time:", end-start
