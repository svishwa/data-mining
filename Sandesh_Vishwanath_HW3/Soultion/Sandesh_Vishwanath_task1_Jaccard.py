import pyspark as py
from collections import defaultdict
import time
import sys

result = list()
resCan = list()
totDict = defaultdict(lambda: 0)
cnt=1


def cal_hash(lst, a, b, m):
    res = []
    for i in lst:
        val = ((a * i) + b) % m
        res.append(val)
    return min(res)


def compute(lst):
    global cnt
    b = 33
    r = 3
    i = 0
    m = 0
    while i < b:
        res_lst = list()
        for k in lst:
            val = k[1][m: m + r]
            #dct[k[0]] = hashFun(val)
            res_lst.append((k[0], val))
        cnt=i
        candidpair(res_lst, dct)
        i += 1
        m += r


def JSim(lst):
    res=[]
    sub=[]
    global resCan
    resCan.sort()
    resdct=dict(lst)
    for i in resCan:
        s1=set(resdct[i[0]])
        s2=set(resdct[i[1]])
        val=len(s1 & s2)*1.0/len(s1 | s2)
        if val >= 0.5:
            sub.append((i[0],i[1]))
            res.append((i[0],i[1],val))
    f=open('LSH.txt','w')
    for i in res:
        f.write(str(i[0])+","+str(i[1])+","+str(i[2])+"\n")
    print len(res)
    return sub


def hashFun(lst):
    val = hash(tuple(lst))
    val %= 500
    return val


def candidpair(lst, cdct):
    global totDict, resCan, cnt
    for k in xrange(0, len(lst)):
        for j in xrange(k + 1, len(lst)):
            #if cdct[lst[k][0]] == cdct[lst[j][0]]:
            if lst[k][1] == lst[j][1] and totDict[tuple(sorted([lst[k][0], lst[j][0]]))] == 0:
                resCan.append(sorted([lst[k][0], lst[j][0]]))
                totDict[tuple(sorted([lst[k][0], lst[j][0]]))] = 1


def actSim(lsttot):
    tot=[]
    for n in xrange(0,len(lsttot)):
        for o in xrange(n+1, len(lsttot)):
            s1=set(lsttot[n][1])
            s2=set(lsttot[o][1])
            val=len(s1 & s2)*1.0/len(s1 | s2)
            if val >= 0.5:
                tot.append((lsttot[n][0], lsttot[o][0]))
    return tot


def fun(x):
    tmp=x.split(",")
    return int(tmp[0]),int(tmp[1])


if __name__ == '__main__':
    sc = py.SparkContext('local[*]')
    start = time.time()
    RDD = sc.textFile(sys.argv[1])
    RDD = RDD.map(lambda x: x.split(","))
    header = RDD.first()
    RDD = RDD.filter(lambda x: x != header)
    RDD = RDD.map(lambda x: (int(x[1]), int(x[0])))
    distinct_user_IDs = RDD.sortByKey().map(lambda x: x[1]).distinct().count()
    distinct_movie_IDs = sorted(RDD.sortByKey().map(lambda x: x[0]).distinct().collect())
    print distinct_user_IDs
    RDD = RDD.combineByKey(lambda x: [x], lambda x, y: x + [y], lambda x, y: x + y).sortByKey()
    i = 1
    a = 25
    b = 31
    p = 20
    m = distinct_user_IDs
    while i <= 99:
        result.append((i, RDD.map(lambda x: cal_hash(x[1], a, b, m)).collect()))
        a += 1
        b += 1
        i += 1

    dct = dict()

    for i in xrange(len(result[1][1])):
        movlst = list()
        for j in result:
            movlst.append(j[1][i])
        dct[distinct_movie_IDs[i]] = movlst
    # print dct[1]
    compute(dct.items())
    listcoll=RDD.collect()
    sub=JSim(listcoll)
    end=time.time()
    print end-start
    # tot=set(sc.textFile("/Users/sv/Downloads/INF 553/Description/data/SimilarMovies.GroundTruth.05.csv")
    #         .map(fun).collect())
    # print "precision:",len(set(sub).intersection(tot))/len(sub)
    # print "recall:",len(set(sub).intersection(tot))/float(len(tot))
    #
