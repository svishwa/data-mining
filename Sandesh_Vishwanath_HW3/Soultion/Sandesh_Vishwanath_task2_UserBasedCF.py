from pyspark import SparkContext
from itertools import combinations
from collections import defaultdict
import math
import time
import sys

weight_dct = defaultdict(lambda: 0)
sim_dct = defaultdict(lambda: 0)
avg_rat_dct=defaultdict(lambda : 0)
rat_usr_dct = defaultdict(dict)
nneighbors_dct = defaultdict(lambda x: 0)


def cal_user_pairs(user_with_ratings):
    arrayToSave = []
    for x, y in combinations(user_with_ratings, 2):
        arrayToSave.append(((x[0], y[0]), (x[1], y[1])))
    return arrayToSave


def cal_weights(usr_pair_with_rat):
    ratings = usr_pair_with_rat[1]
    sum1 = 0
    sum2 = 0
    cnt = 0
    for i in ratings:
        sum1 += i[0]
        sum2 += i[1]
        cnt += 1
    return usr_pair_with_rat[0], (float(sum1)/cnt, float(sum2)/cnt)


def cal_pearson(usr_pair_with_rat):
    global weight_dct
    sum1 = 0
    sum2 = 0
    sum3 = 0
    ratings = usr_pair_with_rat[1]
    for i in ratings:
        sum1 += (i[0] - weight_dct[usr_pair_with_rat[0]][0]) * (i[1] - weight_dct[usr_pair_with_rat[0]][1])
        sum2 += (i[0] - weight_dct[usr_pair_with_rat[0]][0]) * (i[0] - weight_dct[usr_pair_with_rat[0]][0])
        sum3 += (i[1] - weight_dct[usr_pair_with_rat[0]][1]) * (i[1] - weight_dct[usr_pair_with_rat[0]][1])
    denom = (math.sqrt(sum2) * math.sqrt(sum3))
    if denom != 0:
        val = sum1/float(denom)
    else:
        val = 0.0
    return usr_pair_with_rat[0], val


def cal_nearest_neighbors(usr1, usr_sim, n):
    usr_sim = sorted(usr_sim, key=lambda x: x[1], reverse=True)
    return usr1, usr_sim[:n]


def cal_avg_rat(lst):
    sum0=0
    cnt=0
    for i in lst:
        sum0+=i
        cnt+=1
    return sum0/cnt


def prediction(record):
    mv_id = record[0][1]
    act_usr = record[0][0]
    sum0 = 0.0
    sum1 = 0.0
    users = nneighbors_dct[act_usr]

    # avg_rat_dct[i[0]]
    for i in users:
        if mv_id in rat_usr_dct[i[0]].keys():
            sum0 += ((rat_usr_dct[i[0]][mv_id]) - weight_dct[(act_usr,i[0])][1]) * i[1]
            sum1 += math.fabs(i[1])
    if sum1 != 0:
        val = avg_rat_dct[act_usr] + (sum0/float(sum1))
    else:
        val = avg_rat_dct[act_usr]

    return act_usr,(mv_id, val)


def first_user(usr_pair, sim):
    return usr_pair[0], (usr_pair[1], sim)


if __name__ == '__main__':
    sc = SparkContext('local[*]')
    start = time.time()
    RDD = sc.textFile(sys.argv[1])
    RDD = RDD.map(lambda x: x.split(","))
    header = RDD.first()
    RDD = RDD.filter(lambda x: x != header)

    totRDD = RDD.map(lambda x: ((int(x[0]), int(x[1])), float(x[2])))
    predRDD = sc.textFile(sys.argv[2])
    predRDD = predRDD.map(lambda x: x.split(","))
    header1 = predRDD.first()
    predRDD = predRDD.filter(lambda x: x != header1)
    predRDD = predRDD.map(lambda x: ((int(x[0]), int(x[1])), 1.0))
    RDD = totRDD.subtractByKey(predRDD).map(lambda x: (x[0][0], x[0][1], x[1]))

    rat_usr_RDD = RDD.map(lambda x: (x[0], x[1], x[2])).cache()
    rat_usr_lst = rat_usr_RDD.collect()
    for (usr_id, mov_id, rat) in rat_usr_lst:
        rat_usr_dct[usr_id][mov_id] = rat

    avg_rat_RDD = RDD.map(lambda x: (x[0], x[2]))
    print avg_rat_RDD.count()
    avg_rat_RDD = avg_rat_RDD.groupByKey().sortByKey().map(lambda x: (x[0], cal_avg_rat(list(x[1]))))
    avg_rat_dct = avg_rat_RDD.collectAsMap()

    RDD = RDD.map(lambda x: (int(x[1]), [(int(x[0]), float(x[2]))])).reduceByKey(lambda x,y: (x+y)).sortByKey().cache()
    print RDD.count()
    user_pairs = RDD.filter(lambda x: len(x[1]) > 1)
    print user_pairs.count()
    user_pairs = user_pairs.map(lambda x: cal_user_pairs(x[1])).flatMap(lambda xs: [x for x in xs])
    print user_pairs.count()
    user_pairs = user_pairs.groupByKey().sortByKey().cache()
    print user_pairs.count()
    user_pairs = user_pairs.map(lambda x: (x[0], list(x[1]))).cache()
    # print user_pairs.count()
    user_pairs_weights = user_pairs.map(lambda x: cal_weights(x))
    weight_dct = user_pairs_weights.collectAsMap()
    user_sim = user_pairs.map(lambda x: cal_pearson(x))
    sim_dct = user_sim.collectAsMap()
    sim_each_usr = user_sim.map(lambda x: first_user(x[0], x[1])).groupByKey().map(lambda x: (x[0], list(x[1]))).sortByKey().cache()
    sim_each_usr = sim_each_usr.map(lambda x: cal_nearest_neighbors(x[0], x[1], 180))
    nneighbors_dct = sim_each_usr.collectAsMap()
    finRDD = predRDD.map(lambda x: prediction(x))
    RDD_pred_join = finRDD.map(lambda x: ((x[0], x[1][0]), x[1][1]))
    tot_res_RDD = totRDD.join(RDD_pred_join)
    cal_RDD = tot_res_RDD.map(lambda r: ((r[0]), math.fabs(r[1][0] - r[1][1]))).sortByKey()
    MSE = tot_res_RDD.map(lambda r: (r[1][0] - r[1][1]) ** 2).mean()
    print("Root Mean Squared Error = " + str(math.sqrt(MSE)))
    cnt1 = cal_RDD.filter(lambda x: 0 <= x[1] < 1.0).count()
    cnt2 = cal_RDD.filter(lambda x: 1.0 <= x[1] < 2.0).count()
    cnt3 = cal_RDD.filter(lambda x: 2.0 <= x[1] < 3.0).count()
    cnt4 = cal_RDD.filter(lambda x: 3.0 <= x[1] < 4.0).count()
    cnt5 = cal_RDD.filter(lambda x: x[1] >= 4.0).count()
    print ">=0 and < 1:",cnt1
    print ">=1 and < 2:",cnt2
    print ">=2 and < 3:",cnt3
    print ">=3 and < 4:",cnt4
    print ">=4:",cnt5
    resRDD = finRDD.groupByKey().map(lambda x: (x[0], list(x[1]))).sortByKey()
    res_lst = resRDD.collect()
    f = open('User_result.txt','w+')
    for i in res_lst:
        lst = sorted(i[1], key=lambda x:x[0])
        for k in lst:
            f.write(str(i[0])+","+str(k[0])+","+str(k[1])+"\n")
    f.close()
    end= time.time()
    print "time:",end - start