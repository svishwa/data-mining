from pyspark import SparkContext
from pyspark.sql import *
import sys

sc= SparkContext('local')
spark = SparkSession(sc)
DF_rat=spark.read.csv(sys.argv[1],header=True,inferSchema='true')
DF_tag=spark.read.csv(sys.argv[2],header=True,inferSchema='true')
DF_rat=DF_rat.drop('userId','timestamp')
DF_tag=DF_tag.drop('userId','timestamp')
res=DF_tag.join(DF_rat,DF_rat['movieId'] == DF_tag['movieId'],'inner').drop(DF_tag['movieId']).drop(DF_rat['movieId'])
res.createOrReplaceTempView("output")
sqlDF=spark.sql('select tag,avg(rating) as rating_avg from output group by tag')
sqlDF=sqlDF.sort(sqlDF.tag.desc())
sqlDF.coalesce(1).write.format("com.databricks.spark.csv").option("header", "true").save(sys.argv[3]+'Task2_Py.csv')
spark.stop()
sc.stop()
