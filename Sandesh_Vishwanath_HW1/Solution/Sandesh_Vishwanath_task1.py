import pyspark as py
import sys
import csv

sc= py.SparkContext('local')
inp=sys.argv[1]
file=sc.textFile(inp)
RDD_tot=file.map(lambda line:(line.split(",")[1],line.split(",")[2]))
header=RDD_tot.first()
RDD_tot = RDD_tot.filter(lambda line: line != header)
RDD_tot = RDD_tot.map(lambda line: (int(line[0]),float(line[1])))
RDD_tot = RDD_tot.aggregateByKey((0,0), lambda a,b: (a[0] + b, a[1] + 1), lambda a,b: (a[0] + b[0], a[1] + b[1]))
res = RDD_tot.mapValues(lambda v: v[0]/v[1]).collect()
res.sort(key=lambda y:y[0])
details=[('movieId','rating_avg')]
details=details+res
with open(sys.argv[2]+"Task1_Py.csv",'wb') as out:
    csv_out=csv.writer(out)
    for row in details:
        csv_out.writerow(row)
sc.stop()
