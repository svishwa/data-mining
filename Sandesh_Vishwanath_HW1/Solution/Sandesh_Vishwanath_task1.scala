
import org.apache.spark.{SparkConf, SparkContext}

object Sandesh_Vishwanath_task1 {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf()
    conf.setAppName("Datasets Test")
    conf.setMaster("local[*]")
    val sc = new SparkContext(conf)
    val sqlContext= new org.apache.spark.sql.SQLContext(sc)
    import sqlContext.implicits._
    val input=sc.textFile(args(0))
    val header = input.first()
    val data = input.filter(_(0) != header(0))
    var res=data.map(line => line.split(",")).map(line => (line(1).toInt,line(2).toFloat)).cache()
    val df=res.toDF()
    df.createOrReplaceTempView("output")
    var sqlDF=sqlContext.sql("select _1 as movieId,avg(_2) as rating_avg from output group by _1")
    sqlDF=sqlDF.orderBy($"_1")
    sqlDF.coalesce(1).write.format("com.databricks.spark.csv").option("header", "true").save(args(1)+"res.csv")
  }
}