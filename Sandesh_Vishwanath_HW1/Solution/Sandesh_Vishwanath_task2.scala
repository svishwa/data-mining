import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.SparkSession

object Sandesh_Vishwanath_task2 {
  def main(args: Array[String]): Unit = {
    val conf = new SparkConf()
    conf.setAppName("Datasets Test")
    conf.setMaster("local[*]")
    val sc = new SparkContext(conf)
    val sqlContext = new org.apache.spark.sql.SQLContext(sc)
    import sqlContext.implicits._
    val spark = SparkSession.builder().appName("Spark SQL basic example").config("spark.some.config.option", "some-value").getOrCreate()
    var input1 = spark.read.format("csv").option("header","true").load(args(0))
    var input2 = spark.read.format("csv").option("header","true").load(args(1))
    input1=input1.drop("userId","timestamp")
    input2=input2.drop("userId","timestamp")
    var res=input1.join(input2 , input1.col("movieId") === input2.col("movieId"),"inner").drop(input1.col("movieId")).drop(input2.col("movieId"))
    res.createOrReplaceTempView("output")
    var sqlDF=spark.sql("select tag,avg(rating) as rating_avg from output group by tag")
    sqlDF=sqlDF.orderBy($"tag".desc)
    sqlDF.coalesce(1).write.format("com.databricks.spark.csv").option("header", "true").save(args(2)+"res_t2_sc.csv")
  }
}
