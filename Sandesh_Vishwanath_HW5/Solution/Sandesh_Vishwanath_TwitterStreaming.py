import pyspark as py
from collections import defaultdict
import time
from collections import Counter
import sys
import tweepy
from tweepy import OAuthHandler
from tweepy import Stream
import random as r

access_token = "###################"
access_token_secret = "###################"
consumer_key = "###################"
consumer_secret = "###################"


class Mylistener(tweepy.StreamListener):
    htdct = defaultdict(dict)
    twdct = dict()
    avg = 0
    index = 1
    cnt = 0
    check = 0

    def decision(self, probability):
        return r.random() < probability

    def on_status(self, status):
        self.cnt += 1
        if self.index <= 100:
            self.twdct[self.index] = len(status.text)
            hashTags = status.entities.get('hashtags')
            if len(hashTags) != 0:
                htindex = 1
                for i in hashTags:
                    self.htdct[self.index][htindex] = i['text']
                    htindex += 1
            self.index += 1
            if self.cnt == 100:
                popht = defaultdict(lambda x: 0)
                for v in self.htdct.values():
                    for key, val in v.items():
                        popht[val] = popht.get(val, 0) + 1
                c = popht.items()
                c.sort(key=lambda x: x[1], reverse=True)
                print 'Number of the twitter from the beginning : ', self.cnt
                print 'Top 5 hot hashtags:'
                ch = 1
                for k in c:
                    if ch == 6:
                        break
                    ch += 1
                    try:
                        print k[0], " : ", str(k[1])
                    except Exception:
                        pass
                summ = 0
                cnt1 = 1
                for k, v in self.twdct.items():
                    summ += v
                    cnt1 += 1
                self.avg = summ / float(cnt1)
                print 'The average length of the twitter is : ', self.avg
                print('\n')
        else:
            dec = self.decision(100 / float(self.cnt))
            if dec == 1:
                ind = int(round(r.uniform(1, 100)))
                jlen = self.twdct[ind]
                self.twdct[ind] = len(status.text)
                ilen = self.twdct[ind]
                try:
                    del self.htdct[ind]
                except Exception:
                    pass
                hashTags = status.entities.get('hashtags')
                if len(hashTags) != 0:
                    htindex = 1
                    for i in hashTags:
                        self.htdct[ind][htindex] = i['text']
                        htindex += 1
                popht = defaultdict(lambda x: 0)
                for v in self.htdct.values():
                    for key, val in v.items():
                        popht[val] = popht.get(val, 0) + 1
                c = popht.items()
                c.sort(key=lambda x: x[1], reverse=True)
                print 'Number of the twitter from the beginning : ', self.cnt
                print('Top 5 hot hashtags:')
                ch = 1
                for k in c:
                    if ch == 6:
                        break
                    ch += 1
                    try:
                        print k[0], " : ", str(k[1])
                    except Exception:
                        pass
                self.avg += (ilen - jlen) / 100.0
                print 'The average length of the twitter is : ', self.avg
                print('\n')

    def on_error(self, status):
        if status == 420:
            return False
        # print status.text


if __name__ == '__main__':
    auth = OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)
    stream = Stream(auth, Mylistener())
    stream.filter(languages=['en'], track=['FIFA', 'CR7', 'Anime', 'CR7', 'Dogs', 'Cricket', 'Machine Learning', 'Artificial Intelligince'])
