import twitter4j.StatusListener
import twitter4j._
import scala.collection.mutable.Map
import scala.util.Try
import twitter4j.TwitterStreamFactory
import twitter4j.FilterQuery
import twitter4j.HashtagEntity
import twitter4j.Status
import twitter4j.conf.ConfigurationBuilder
import scala.collection.immutable.ListMap


object Sandesh_Vishwanath_TwitterStreaming {

  var cnt = 0
  var htdct: Map[Int, Array[String]] = Map()
  var twdct: Map[Int, Int] = Map()
  var avrg = 0.0
  var index = 1
  var check = 0

  def main(args: Array[String]): Unit = {
    val cb = new ConfigurationBuilder()
    cb.setDebugEnabled(true)
      .setOAuthConsumerKey("###################")
      .setOAuthConsumerSecret("###################")
      .setOAuthAccessToken("###################")
      .setOAuthAccessTokenSecret("###################")


    def simpleStatusListener = new StatusListener() {
      def onStatus(status: Status) {
        cnt += 1
        if (index <= 100) {
          twdct += (index -> status.getText().length())
          val hashTags = status.getHashtagEntities()
          if (hashTags.length != 0){
            for (myString <- hashTags)
              if (htdct.contains(index)) {
                var arr = htdct(index)
                var arr1 = arr :+ myString.getText
                htdct += (index -> arr1)
              }
              else {
                htdct += (index -> Array(myString.getText))
              }
          }
          index += 1
          if (cnt == 100) {
            var popht: Map[String, Int] = Map()
            for ((k,v) <- htdct){
              for (tag <- v){
                if (popht.contains(tag)){
                  var value = popht(tag) + 1
                  popht += (tag -> value)
                }
                else
                {
                  popht += (tag -> 1)
                }
              }
            }
            var first5 = popht.toSeq.sortBy(-_._2)
            println("Number of the twitter from the beginning : "+ cnt)
            println("Top 5 hot hashtags:")
            first5 = first5.take(5)
            for((k,v) <- first5){
              println(k+" : "+v)
            }
            var summ = 0
            var cnt1 = 1
            for((k,v) <- twdct) {
              summ += v
              cnt1 += 1
            }
            avrg = summ / cnt1.toFloat
            println("The average length of the twitter is :"+avrg)
            println(' ')
            println(' ')
          }
        }
        else {
          val r = scala.util.Random
          if ((r.nextFloat < 100.0/cnt) == true) {
            val r = new scala.util.Random
            var ind = 20 + r.nextInt((100 - 1) + 1)
            var jlen = twdct(ind)
            twdct(ind) = status.getText.length
            var ilen = twdct(ind)
            Try {
              htdct -= ind
            }
            val hashTags = status.getHashtagEntities()
            if (hashTags.length != 0) {
              for (myString <- hashTags)
                if (htdct.contains(index)) {
                  var arr = htdct(index)
                  var arr1 = arr :+ myString.getText
                  htdct += (index -> arr1)
                }
                else {
                  htdct += (index -> Array(myString.getText))
                }
            }
            var popht: Map[String, Int] = Map()
            for ((k,v) <- htdct){
              for (tag <- v){
                if (popht.contains(tag)){
                  var value = popht(tag) + 1
                  popht += (tag -> value)
                }
                else
                {
                  popht += (tag -> 1)
                }
              }
            }
            var first5 = popht.toSeq.sortBy(-_._2)
            println("Number of the twitter from the beginning : "+ cnt)
            println("Top 5 hot hashtags:")
            first5 = first5.take(5)
            for((k,v) <- first5){
              println(k+" : "+v)
            }
            avrg += (ilen - jlen) / 100.0
            println("The average length of the twitter is : "+ avrg)
            println(' ')
            println(' ')
          }
        }

      }

      def onDeletionNotice(statusDeletionNotice: StatusDeletionNotice) = {}

      def onTrackLimitationNotice(numberOfLimitedStatuses: Int) = {}

      def onScrubGeo(userId: Long, upToStatusId: Long) = {}

      def onException(ex: Exception) = {}

      def onStallWarning(warning: StallWarning) = {}
    }

    val tf = new TwitterStreamFactory(cb.build())
    val twitter = tf.getInstance()
    twitter.addListener(simpleStatusListener)
    twitter.filter("FIFA","CR7","Dogs","Cats","WorldCup","Anime")

  }
}

