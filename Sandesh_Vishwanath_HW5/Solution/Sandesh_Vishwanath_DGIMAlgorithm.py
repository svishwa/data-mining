import pyspark as py
from pyspark.streaming import StreamingContext
from collections import defaultdict
import time
import math
from collections import OrderedDict
import sys

dct = defaultdict(list)
bit_cnt_1 = 0
cnt_lst = list()
tot_bit_cnt = 0
N = 1000
n = int(math.log(N, 2))
counter = 0
bck_sz = OrderedDict()

while counter <= n:
    bck_sz[2 ** counter] = 0
    counter += 1


def add_to_bucket(bit_pos):
    global dct, N
    dct[1].append(bit_pos % N)
    check_for_size()


def check_for_size():
    global bck_sz, dct
    lst = bck_sz.keys()
    for i in bck_sz.keys():
        if len(dct[i]) <= 2:
            break
        else:
            del dct[i][0]
            val = dct[i].pop(0)
            if i != lst[-1]:
                dct[i * 2].append(val)


def func(s):
    global bit_cnt_1, tot_bit_cnt, N, dct, cnt_lst
    flag = 0
    bits = [int(j) for j in s.collect()]
    for i in bits:
        cnt_lst.append(i)
        tot_bit_cnt = tot_bit_cnt + 1
        if tot_bit_cnt <= N:
            if i == 0:
                pass
            else:
                bit_cnt_1 += 1
                add_to_bucket(tot_bit_cnt)
                if tot_bit_cnt % N == 0:
                    est_ones = 0
                    p = dct.keys()
                    for key in p[:len(p) - 1]:
                        est_ones += key * len(dct[key])
                    largest_bucket = p[len(p) - 1] * len(dct[p[len(p) - 1]])
                    est_ones += (largest_bucket / 2)
                    print "Estimated number of ones in the last 1000 bits: ", est_ones
                    print "Actual number of ones in the last 1000 bits: ", bit_cnt_1
                    print("\n")
                    bit_cnt_1 = 0
        else:
            flag = 1
            cur_time = tot_bit_cnt % N
            for k in dct.keys():
                for item_stamp in dct[k]:
                    if item_stamp == cur_time:
                        del dct[k]
            if i == 0:
                pass
            else:
                add_to_bucket(tot_bit_cnt)
    r = 0
    if flag == 1:
        for l in xrange(len(bits)):
            cnt_lst.pop(0)
        for m in cnt_lst:
            if m == 1:
                r += 1
        est_ones = 0
        p = dct.keys()
        for key in p[:len(p) - 1]:
            est_ones += key * len(dct[key])
        largest_bucket = p[len(p) - 1] * len(dct[p[len(p) - 1]])
        est_ones += (largest_bucket / 2)
        print "Estimated number of ones in the last 1000 bits: ", est_ones
        print "Actual number of ones in the last 1000 bits: ", r
        print("\n")


if __name__ == '__main__':
    sc = py.SparkContext('local[*]')
    ssc = StreamingContext(sc, 10)
    lines = ssc.socketTextStream('localhost', 9999)
    sc.setLogLevel(logLevel='OFF')
    lines.foreachRDD(lambda stream: func(stream))
    ssc.start()
    ssc.awaitTermination()
