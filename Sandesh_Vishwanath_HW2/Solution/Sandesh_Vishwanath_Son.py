import pyspark as py
from collections import *
import math
import time

import sys

bucket = defaultdict(lambda: 0)
bit_map = defaultdict(lambda: 0)
part_thresh = 1
freq_lst = []
p1_itemsets = []


def phase1(partition):
    global part_thresh, bit_map, p1_itemsets, freq_lst
    partition = list(partition)
    item_cnt = defaultdict(lambda: 0)
    for i in partition:
        for k in i:
            item_cnt[k] += 1

    lst = set()

    for k in item_cnt.keys():
        if item_cnt[k] >= part_thresh:
            freq_lst.append(((k,), item_cnt[k]))
            lst.add(frozenset([k]))
    size = 2

    while 1:
        c_lst = set([i.union(j) for i in lst for j in lst if len(i.union(j)) == size])
        l_lst = list()
        pos_lst = set()
        cnt = defaultdict(lambda: 0)

        for i in partition:
            for j in c_lst:
                if set(j).issubset(i):
                    cnt[j] += 1
                if cnt[j] >= part_thresh:
                        l_lst.append((tuple(sorted(set(j))), cnt[j]))
                        pos_lst.add(j)

        lst = pos_lst
        freq_lst += l_lst
        if not l_lst:
            break
        size += 1

    return freq_lst


def phase2(partition):
    global p1_itemsets, part_thresh
    partition = list(partition)
    cnt = defaultdict(lambda: 0)
    for i in partition:
        for j in p1_itemsets:
            if type(j[0]) != int:
                temp = set()
                for t in set(j[0]):
                    temp.add(t)
                if temp.issubset(set(i)):
                    cnt[j[0]] += 1
            else:
                if j[0] in set(i):
                    cnt[j[0]] += 1
    return cnt.items()


if __name__ == '__main__':
    sc = py.SparkContext(sys.argv[4])
    start = time.time()
    RDD = sc.textFile(sys.argv[2])
    RDD = RDD.map(lambda x: x.split(","))
    header = RDD.first()
    RDD = RDD.filter(lambda x: x != header)
    if int(sys.argv[1]) == 1:
        RDD = RDD.map(lambda x: (int(x[0]), int(x[1])))
    else:
        RDD = RDD.map(lambda x: (int(x[1]), int(x[0])))
    support = sys.argv[3]
    RDD = RDD.combineByKey(lambda x: [x], lambda x, y: x + [y], lambda x, y: x + y)
    RDD = RDD.map(lambda y: list(set(y[1]))).cache()

    part_thresh = math.floor((1 / float(RDD.getNumPartitions())) * float(support))

    p1_itemsets = RDD.mapPartitions(phase1).reduceByKey(lambda x, y: 1).distinct().collect()

    p2_itemsets = RDD.mapPartitions(phase2).reduceByKey(lambda x, y: x+y)

    print "Length is " + str(len(p2_itemsets.collect()))


    output = p2_itemsets.filter(lambda x: x[1] >= int(support))
    result = output.map(lambda line: line[0]).collect()

    print "Total output is "

    result.sort(key=lambda t: t)
    result.sort(key=lambda t: len(t))
    end = time.time()

    print RDD.getNumPartitions(), part_thresh
    print len(result)
    print end - start
    with open('Sandesh_Vishwanath_SON_MovieLens.Big.case2-2800.txt', 'w') as the_file:
        if len(result[0]) != 1:
            the_file.write(str(result[0]))
        else:
            the_file.write("("+str(int(result[0][0]))+")")
        for i in xrange(1,len(result)):
            if len(result[i-1]) == len(result[i]):
                if len(result[i]) == 1:
                    the_file.write(", ("+str(int(result[i][0]))+")")
                else:
                    the_file.write(", "+str(result[i]))
            else:
                the_file.write("\n\n"+str(result[i]))
    the_file.close()
