from __future__ import print_function
import pyspark as py
from itertools import combinations
from collections import defaultdict
from collections import OrderedDict
import time
import sys

usr_mov_dct = dict()
usr_pair = list()
node_for_graph = list()
parent = defaultdict(lambda: 0)
edge_bw = defaultdict(lambda: 0)


def gen_usr_pair(lst):
    global usr_pair
    usr_pair = list(combinations(lst, 2))


def node_creation_check():
    global usr_pair, usr_mov_dct, node_for_graph
    for i in usr_pair:
        n1 = usr_mov_dct[i[0]]
        n2 = usr_mov_dct[i[1]]
        if len(set(n1).intersection(set(n2))) >= 9:
            node_for_graph.append((i[0], i[1]))
            node_for_graph.append((i[1], i[0]))


def BFS(dct):
    global edge_bw
    for k in dct.keys():
        parents_dct = defaultdict(list)
        node_flow_dct = defaultdict(lambda: 0)
        for i in range(1, len(dct) + 1):
            node_flow_dct[i] = 1
        node_val_dct = defaultdict(lambda: 0)
        level = defaultdict(lambda: 0)
        rev_level = defaultdict(list)
        visited = [False] * (len(dct) + 1)
        queue = []
        queue.append(k)
        level[k] = 1
        root = k
        visited[k] = True
        while queue:
            s = queue.pop(0)
            for i in dct[s]:
                if not visited[i]:
                    parents_dct[i].append(s)
                    node_val_dct[i] = 1
                    level[i] = level[s] + 1
                    queue.append(i)
                    visited[i] = True
                else:
                    if level[i] > level[s]:
                        node_val_dct[i] += 1
                        parents_dct[i].append(s)
        for key, value in sorted(level.iteritems()):
            rev_level[value].append(key)
        l = len(rev_level)
        while l != 0:
            for m in rev_level[l]:
                par_lst = parents_dct[m]
                weight = node_val_dct[m]
                for p in par_lst:
                    val = node_flow_dct[m] / float(weight)
                    edge_bw[tuple(sorted([m, p]))] += val
                    node_flow_dct[p] += val
            l -= 1


if __name__ == '__main__':
    start = time.time()
    sc = py.SparkContext('local[*]')
    RDD = sc.textFile(sys.argv[1])
    RDD = RDD.map(lambda x: x.split(","))
    header = RDD.first()
    RDD = RDD.filter(lambda x: x != header)
    RDD = RDD.map(lambda x: (int(x[0]), int(x[1])))
    distinct_user_IDs = sorted(RDD.sortByKey().map(lambda x: x[0]).distinct().collect())
    RDD = RDD.combineByKey(lambda x: [x], lambda x, y: x + [y], lambda x, y: x + y).sortByKey()
    usr_mov_dct = RDD.collectAsMap()
    gen_usr_pair(distinct_user_IDs)
    node_creation_check()
    nodes_RDD = sc.parallelize(node_for_graph).groupByKey().map(lambda x: (x[0], list(x[1])))
    adj_list_dict = nodes_RDD.collectAsMap()
    BFS(adj_list_dict)
    for k in edge_bw.keys():
        edge_bw[k] /= 2.0
    e = OrderedDict(sorted(edge_bw.items(), key=lambda t: t[0]))
    fp = open('HW4.txt','w+')
    for i in e.items():
        fp.write("("+ str(i[0][0]) + "," + str(i[0][1]) + "," + str(i[1]) + ")\n")
    fp.close()

    end = time.time()
    print(end - start)
