from __future__ import print_function
import pyspark as py
from itertools import combinations
from collections import defaultdict
from collections import OrderedDict
import networkx as nx
import time
import sys

usr_mov_dct = dict()
usr_pair = list()
node_for_graph = list()
parent = defaultdict(lambda: 0)
edge_bw = defaultdict(lambda: 0)
graph = nx.Graph()
final = list()
deg = defaultdict(lambda: 0)
graph1 = nx.Graph()


def compute_modularity(lst, m):
    global deg, graph1
    mat = nx.to_numpy_matrix(graph1)
    summ = 0
    for o in lst:
        o = list(o)
        for i in range(0, len(o)):
            ki = deg[o[i]]
            for j in range(i, len(o)):
                Aij = mat[o[i] - 1, o[j] - 1]
                kj = deg[o[j]]
                summ += (Aij - ((ki * kj) / (2.0 * m)))
    summ /= (2.0 * m)
    return summ


def community_detect(dct):
    global graph, graph1
    m = graph1.number_of_edges()
    prev_com = nx.number_connected_components(graph)
    mod = -2
    max_edges = list()
    for key, value in sorted(dct.items(), key=lambda (k, v): (v, k), reverse=True):
        max_edges.append((key, value))

    while True:
        graph.remove_edge(*max_edges[0][0])
        del max_edges[0]
        new_com = nx.number_connected_components(graph)
        if new_com != prev_com:
            prev_com = new_com
            lst = list(nx.connected_components(graph))
            new_mod = compute_modularity(lst, m)
            print(new_com, len(max_edges), new_mod)
            if new_mod > mod:
                mod = new_mod
                res = lst
        if len(max_edges) == 0:
            break
        else:
            continue
    fp = open('HW4_task2.txt', 'w+')
    res = list(sorted(list(c)) for c in res)
    res = sorted(res)
    for i in res:
        fp.write(str(i) + "\n")
    fp.close()


def create_graph():
    global graph, node_for_graph
    graph.add_edges_from(node_for_graph)


def gen_usr_pair(lst):
    global usr_pair
    usr_pair = list(combinations(lst, 2))


def node_creation_check():
    global usr_pair, usr_mov_dct, node_for_graph
    for i in usr_pair:
        n1 = usr_mov_dct[i[0]]
        n2 = usr_mov_dct[i[1]]
        if len(set(n1).intersection(set(n2))) >= 9:
            node_for_graph.append((i[0], i[1]))
            node_for_graph.append((i[1], i[0]))


def BFS(dct):
    global edge_bw
    for k in dct.keys():
        parents_dct = defaultdict(list)
        node_flow_dct = defaultdict(lambda: 0)
        for i in range(1, len(dct) + 1):
            node_flow_dct[i] = 1
        node_val_dct = defaultdict(lambda: 0)
        level = defaultdict(lambda: 0)
        rev_level = defaultdict(list)
        visited = [False] * (len(dct) + 1)
        queue = []
        queue.append(k)
        level[k] = 1
        visited[k] = True
        while queue:
            s = queue.pop(0)
            for i in dct[s]:
                if not visited[i]:
                    parents_dct[i].append(s)
                    node_val_dct[i] = 1
                    level[i] = level[s] + 1
                    queue.append(i)
                    visited[i] = True
                else:
                    if level[i] > level[s]:
                        node_val_dct[i] += 1
                        parents_dct[i].append(s)
        for key, value in sorted(level.iteritems()):
            rev_level[value].append(key)
        l = len(rev_level)
        while l != 0:
            for m in rev_level[l]:
                par_lst = parents_dct[m]
                weight = node_val_dct[m]
                for p in par_lst:
                    val = node_flow_dct[m] / float(weight)
                    edge_bw[tuple(sorted([m, p]))] += val
                    node_flow_dct[p] += val
            l -= 1


if __name__ == '__main__':
    start = time.time()
    sc = py.SparkContext('local[*]')
    RDD = sc.textFile(sys.argv[1])
    RDD = RDD.map(lambda x: x.split(","))
    header = RDD.first()
    RDD = RDD.filter(lambda x: x != header)
    RDD = RDD.map(lambda x: (int(x[0]), int(x[1])))
    distinct_user_IDs = sorted(RDD.sortByKey().map(lambda x: x[0]).distinct().collect())
    RDD = RDD.combineByKey(lambda x: [x], lambda x, y: x + [y], lambda x, y: x + y).sortByKey()
    usr_mov_dct = RDD.collectAsMap()
    gen_usr_pair(distinct_user_IDs)
    node_creation_check()
    graph1 = nx.Graph()
    graph1.add_edges_from(node_for_graph)
    nodes_RDD = sc.parallelize(node_for_graph).groupByKey().map(lambda x: (x[0], list(x[1])))
    adj_list_dict = nodes_RDD.collectAsMap()
    BFS(adj_list_dict)
    for k in edge_bw.keys():
        edge_bw[k] /= 2.0
    e = OrderedDict(sorted(edge_bw.items(), key=lambda t: t[0]))
    create_graph()
    graph1 = nx.Graph(graph)
    deg = dict(graph1.degree(graph1.nodes()))
    community_detect(e)
    end = time.time()
    print(end - start)
