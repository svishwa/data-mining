import pyspark as py
import networkx as nx
import networkx.algorithms.community.centrality as c
import time
import sys
from itertools import combinations

usr_mov_dct = dict()
usr_pair = list()
node_for_graph = list()


def gen_usr_pair(lst):
    global usr_pair
    usr_pair = list(combinations(lst, 2))


def node_creation_check():
    global usr_pair, usr_mov_dct, node_for_graph
    for i in usr_pair:
        n1 = usr_mov_dct[i[0]]
        n2 = usr_mov_dct[i[1]]
        if len(set(n1).intersection(set(n2))) >= 9:
            node_for_graph.append((i[0], i[1]))
            node_for_graph.append((i[1], i[0]))


if __name__ == '__main__':
    start = time.time()
    sc = py.SparkContext('local[*]')
    RDD = sc.textFile(sys.argv[1])
    RDD = RDD.map(lambda x: x.split(","))
    header = RDD.first()
    RDD = RDD.filter(lambda x: x != header)
    RDD = RDD.map(lambda x: (int(x[0]), int(x[1])))
    distinct_user_IDs = sorted(RDD.sortByKey().map(lambda x: x[0]).distinct().collect())
    RDD = RDD.combineByKey(lambda x: [x], lambda x, y: x + [y], lambda x, y: x + y).sortByKey()
    usr_mov_dct = RDD.collectAsMap()
    gen_usr_pair(distinct_user_IDs)
    node_creation_check()
    graph = nx.Graph()
    graph.add_edges_from(node_for_graph)
    comp = c.girvan_newman(graph)
    top_level_communities = next(comp)
    next_level_communities = next(comp)
    res = sorted(map(sorted, next_level_communities))
    fp = open('Bonus.txt','w+')
    for j in res:
        fp.write(str(j)+"\n")
    fp.close()
    end = time.time()
    print end-start
